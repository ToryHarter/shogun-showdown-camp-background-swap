﻿using HarmonyLib;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace CampBackgroundSwap.Patches
{
    static class CombatSceneManagerPatch
    {
        [HarmonyPatch(typeof(Room), "Initialize", MethodType.Normal)]
        static class Room_Start_Patch
        {
            static void Prefix(Room __instance)
            {
                var selectedLocation = LocationUtility.LocationKeys[ModModule.SettingsConfig.SelectedBackground];

                if (__instance.name != "Camp_Room(Clone)") return;

                Environment bambooEnvironment = null;
                var mapLocations = Progression.Instance.map.MapLocations;
                foreach (var mapLocation in mapLocations)
                {
                    var rooms = new List<Room>();
                    foreach (SubLocation subLocation in mapLocation?.location?.subLocations ?? (new List<SubLocation>()).ToArray())
                    {
                        rooms.Add(subLocation.rooms.FirstOrDefault());
                    }

                    foreach (var room in rooms)
                    {
                        if (room.gameObject.name == selectedLocation.Item1)
                        {
                            foreach (Transform child in room.transform)
                            {
                                if (child.name == selectedLocation.Item2)
                                {
                                    var components = child.GetComponents(typeof(Component));
                                    foreach (var component in components)
                                    {
                                        if (component.GetType().Name == "Environment")
                                        {
                                            bambooEnvironment = GameObject.Instantiate((Environment)component);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                var campEnvironment = __instance.GetComponentInChildren<Environment>();
                if (campEnvironment.gameObject.name == "CampEnv")
                {

                    foreach (Transform child in campEnvironment.transform)
                    {
                        GameObject.Destroy(child.gameObject);
                    }

                    foreach (Transform child in bambooEnvironment.transform)
                    {
                        child.parent = campEnvironment.gameObject.transform;
                    }
                }
            }
        }


    }
}
