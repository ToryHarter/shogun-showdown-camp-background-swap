﻿using System;
using System.Collections.Generic;

namespace CampBackgroundSwap
{
    public enum BackgroundLocation
    {
        Camp = 0,
        BambooGrove = 1,
        Graveyard = 2,
        WaterfallCaves = 3,
        TempleGate = 4,
        HideyoshiKeep = 5,
        HotSprings = 6,
        NobunagaFortress = 7,
        PortCity = 8,
        AncientBattleground = 9,
        TheatreOfIllusions = 10,
        ShogunCastlePhase1 = 11,
        ShogunCastlePhase2 = 12,
        ShogunCastlePhase3 = 13,
    }
    static class LocationUtility
    {

        public static Dictionary<BackgroundLocation, Tuple<string, string>> LocationKeys = new Dictionary<BackgroundLocation, Tuple<string, string>>()
        {
            { BackgroundLocation.Camp, new Tuple<string, string> ("Camp_Room", "CampEnv" ) },
            { BackgroundLocation.BambooGrove, new Tuple<string, string> ("BambooGrove_Daisuke_BossRoom", "BambooGrove" ) },
            { BackgroundLocation.Graveyard, new Tuple<string, string> ("Graveyard_BossRoom", "GraveyardCombatEnv" ) },
            { BackgroundLocation.WaterfallCaves, new Tuple<string, string> ("WaterfallCaves_BossRoom", "WaterfallCavesEnvironment" ) },
            { BackgroundLocation.TempleGate, new Tuple<string, string> ("TempleGate_BossRoom", "TempleGateEnvironment" ) },
            { BackgroundLocation.HideyoshiKeep, new Tuple<string, string> ("HideyoshiKeep_BossRoom", "HideyoshiKeepCombatEnv" ) },
            { BackgroundLocation.HotSprings, new Tuple<string, string> ("HotSprings_BossRoom", "HotSpringsCombatEnv" ) },
            { BackgroundLocation.NobunagaFortress, new Tuple<string, string> ("NobunagaFortress_BossRoom", "NobunagaFortressEnvironment" ) },
            { BackgroundLocation.PortCity, new Tuple<string, string> ("PortCity_BossRoom", "PortCityEnvironment" ) },
            { BackgroundLocation.AncientBattleground, new Tuple<string, string> ("AncientBattleground_BossRoom", "AncientBattlegroundCombatEnv" ) },
            { BackgroundLocation.TheatreOfIllusions, new Tuple<string, string> ("TheatreOfIllusions_BossRoom", "TheatreOfIllusionsCombatEnv" ) },
            { BackgroundLocation.ShogunCastlePhase1, new Tuple<string, string> ("ShogunCastle_BossRoom", "ShogunCastleCombatEnv" ) },
            { BackgroundLocation.ShogunCastlePhase2, new Tuple<string, string> ("ShogunCastle_BossRoom", "ShogunCastlePhase2CombatEnv" ) },
            { BackgroundLocation.ShogunCastlePhase3, new Tuple<string, string> ("ShogunCastle_BossRoom", "ShogunCastlePhase3CombatEnv" ) },
        };
    }
}
