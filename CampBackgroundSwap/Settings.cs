﻿using System;
using UnityModManagerNet;

namespace CampBackgroundSwap
{
    public class Settings : UnityModManager.ModSettings, IDrawable
    {
        [Draw(Label = "Background Location")]
        public BackgroundLocation SelectedBackground = new BackgroundLocation();

        public void OnChange() {}

        public override void Save(UnityModManager.ModEntry modEntry)
        {
            try
            {
                ModModule.Mod.Logger.Log("Saving UnityModManager settings.");

                Save(this, modEntry);

                ModModule.Mod.Logger.Log("Successfully saved UnityModManager settings.");
            }
            catch (Exception e)
            {
                ModModule.Mod.Logger.Error($"Error saving UnityModManager Settings: {e.InnerException}");
            }
        }
    }
}
