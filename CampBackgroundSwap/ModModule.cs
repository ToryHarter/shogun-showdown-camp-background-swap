﻿using HarmonyLib;
using System;
using System.Reflection;
using UnityModManagerNet;

namespace CampBackgroundSwap
{
    static class ModModule
    {
        public static UnityModManager.ModEntry Mod;
        public static bool Enabled;
        public static Settings SettingsConfig;

        static bool Load(UnityModManager.ModEntry modEntry)
        {
            try
            {
                Mod = modEntry;
                modEntry.OnToggle = OnToggle;
                modEntry.OnGUI = OnGUI;
                modEntry.OnSaveGUI = OnSaveGUI;

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        static bool OnToggle(UnityModManager.ModEntry modEntry, bool value)
        {
            try
            {
                var harmony = new Harmony("ShogunShowdownCampBackgroundSwap");

                if (value)
                {
                    SettingsConfig = new Settings();
                    SettingsConfig = Settings.Load<Settings>(modEntry);

                    harmony.PatchAll(Assembly.GetExecutingAssembly());

                    Mod.Logger.Log("Mod Enabled.");
                }
                else
                {
                    harmony.UnpatchAll("ShogunShowdownCampBackgroundSwap");

                    Mod.Logger.Log("Mod Disabled.");
                }

                Enabled = value;
                return true;
            }
            catch (Exception e)
            {
                Mod.Logger.Error($"Error toggling mod: {e.InnerException}");

                return false;
            }
        }

        static void OnGUI(UnityModManager.ModEntry modEntry)
        {
            SettingsConfig.Draw(modEntry);
        }

        static void OnSaveGUI(UnityModManager.ModEntry modEntry)
        {
            SettingsConfig.Save(modEntry);
        }
    }
}
